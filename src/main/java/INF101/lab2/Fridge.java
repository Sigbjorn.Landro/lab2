package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    private ArrayList<FridgeItem> inFridge;
    private int max_size;

    public Fridge() {
        this(20);
    }

    public Fridge(int size) {
        this.max_size = size;
        this.inFridge = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        // Returns the number of items currently in the fridge
        return inFridge.size();
    }

    @Override
    public int totalSize() {
        // Returns the total number of items there is space for in the fridge
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // Place a food item in the fridge.
        // Items can only be placed in the fridge if there is space,
        // if successful return true and return false if not
        if (inFridge.size() < max_size) {
            inFridge.add(item);
            return true;
        } else return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // Remove item (given as parameter) from fridge or throw if the item isn't in fridge
        if (inFridge.contains(item)) {
            inFridge.remove(item);
        } else throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        // Remove all items from the fridge
        inFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // Remove all items that have expired from the fridge and returns list of expired items
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for (int i = 0; i < inFridge.size(); i++) {
            if (inFridge.get(i).hasExpired()) {
                expiredItems.add(inFridge.get(i));
            }
        }
        inFridge.removeAll(expiredItems);
        return expiredItems;
    }
    
}
